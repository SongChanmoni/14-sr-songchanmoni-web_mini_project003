import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteArticle, fetchArticle } from "../redux/Action/ArticleAction";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import strings from "../locale/string";

export default function Home() {

  return (
    <Container className="my-3">
      <Row>
        <Col md="3">
          <Card>
            <Card.Img variant="top"  src="https://image.freepik.com/free-vector/website-user-login-page-template-design_1017-30786.jpg" />
            <Card.Body>
              <Card.Title>Login</Card.Title>
              <Button variant="success">Login with Google</Button>
            </Card.Body>
          </Card>
        </Col>
        <Col md="9">
          <h1>{strings.Category}</h1>
        </Col>
             
      </Row>
      
    </Container>
  );
}
