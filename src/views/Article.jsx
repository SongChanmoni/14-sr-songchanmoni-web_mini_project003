import React from 'react'
import { Form, Button, Container, Row, Col } from 'react-bootstrap'
import strings from '../locale/string'
export default function Article() {
  return (
    <div>
      <Container>
        <Row>
          <Col md="8" style={{ marginTop: "20px" }}>
            <Form>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>{strings.title}</Form.Label>
                <Form.Control type="text" placeholder="Title" />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>{strings.Author}</Form.Label>
                <Form.Control type="text" placeholder="Author" />
              </Form.Group>
              <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                <Form.Label>{strings.description}</Form.Label>
                <Form.Control as="textarea" rows={3} />
              </Form.Group>
              <Button variant="primary" type="submit">
                {strings.save}
              </Button>
          </Form>
        </Col>
        <Col md="3" style={{ marginTop: "40px" }}>
          <img width="400px" src="https://designshack.net/wp-content/uploads/placeholder-image.png"/>
          
        </Col>
      </Row>

    </Container>

    </div >
  )
}
