import { Switch, BrowserRouter, Route } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import Article from './views/Article';
import Author from './views/Author';
import Category from './views/Category';
import Home from './views/Home';
import Menu from './Components/Menu';
import React,{ Component } from 'react';
import strings from './locale/string';

export default class App extends Component {
  onChangeLanguage = (str) => {
    strings.setLanguage(str);
    this.setState({});
  }

  render() {
    return (
      <Container>
        <BrowserRouter>
          <Menu onChangeLanguage={this.onChangeLanguage} />
          <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/author' component={Author} />
            <Route path='/article' component={Article} />
            <Route path='/category' component={Category} />
            <Route path='/update/article/:id' component={Article} />
          </Switch>
        </BrowserRouter>

      </Container>
    )
  }
}

