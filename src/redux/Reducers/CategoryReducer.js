const initialState = {
    categories: [],
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

    case "FETCH_CATEGORY":
        return { ...state, categories: payload }
    default:
        return state
    }
}