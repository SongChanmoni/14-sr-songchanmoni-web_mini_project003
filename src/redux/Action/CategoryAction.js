import api from "../../API/api"

export const fetchCategory = () => async dp => {

    let response = await api.get('/category')

    return dp({
        type: "FETCH_CATEGORY",
        payload: response.data.data
    })

}