
import api from "../../API/api";

export const fetchArticle = () => async dp =>{

    let response = await api.get('/articles');

    return dp({
        type: "FETCH_ARTICLE",
        payload: response.data.data
    })
}


export const uploadImage = (file) => async dp => {

    let formData = new FormData()
    formData.append('image', file)

    let response = await api.post('/images', formData)

    return dp({
        type: "UPLOAD_IMG",
        payload: response.data.url
    })
}



