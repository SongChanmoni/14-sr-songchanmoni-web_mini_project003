import React from 'react'
import { Navbar, Nav, NavDropdown, FormControl, Form, Button, Container } from 'react-bootstrap'
import { NavLink } from "react-router-dom";
import strings from "../locale/string"; 
export default function Menu({ onChangeLanguage }) {
      
    return (
        <div className="container">
            <Navbar bg="success" expand="lg">
                <Navbar.Brand href="#home">AMS Redux</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link as={NavLink} to='/'>{strings.Home}</Nav.Link>
                        <Nav.Link as={NavLink} to='/author'>{strings.Author}</Nav.Link>
                        <Nav.Link as={NavLink} to='/article'>{strings.Article}</Nav.Link>
                        <Nav.Link as={NavLink} to='/category'>{strings.Category}</Nav.Link>
                        <NavDropdown title={strings.lang} id="basic-nav-dropdown">
                            <NavDropdown.Item href="#action/3.1" onClick={() => onChangeLanguage("en")}> {strings.en}</NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.2" onClick={() => onChangeLanguage("kh")}>{strings.kh}</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                    <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                        <Button variant="outline-ligh">Search</Button>
                    </Form>
                </Navbar.Collapse>
            </Navbar>
        </div>
    )
}
